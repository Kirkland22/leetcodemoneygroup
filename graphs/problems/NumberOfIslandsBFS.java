package graphs.problems;

import java.util.*;

// https://leetcode.com/problems/number-of-islands/
public class NumberOfIslandsBFS {
    int numberOfIslands = 0;
    Set<String> visited = new HashSet<>();
    int maxXCoordinate;
    int maxYCoordinate;
    public int numIslandsBFS(char[][] grid) {
        maxXCoordinate = grid.length;
        maxYCoordinate = grid[0].length;
        Deque<Coordinate> coordinateDeque = new ArrayDeque<>();
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[0].length; y++) {
                Coordinate currentSpot = new Coordinate(x, y);
                if(grid[x][y] == '1' && !visited.contains(currentSpot.x + "-" + currentSpot.y)) {
                   coordinateDeque.addLast(currentSpot);
                   numberOfIslands++;
                   while(!coordinateDeque.isEmpty()) {
                       Coordinate queueCoordinate = coordinateDeque.removeFirst();
                       if(!isValidCoordinate(queueCoordinate) || grid[queueCoordinate.x][queueCoordinate.y] == '0' || visited.contains(queueCoordinate.x + "-" + queueCoordinate.y)) {
                           continue;
                       }
                        visited.add(queueCoordinate.x + "-" + queueCoordinate.y);

                       // Is spot left of me part of my island
                        Coordinate leftSpot = new Coordinate(queueCoordinate.x-1,queueCoordinate.y);
                        coordinateDeque.addLast(leftSpot);
                        // Is spot right of me part of my island
                        Coordinate rightSpot = new Coordinate(queueCoordinate.x+1 ,queueCoordinate.y);
                        coordinateDeque.addLast(rightSpot);
                        // Is spot below me part of my island
                        Coordinate belowSpot = new Coordinate(queueCoordinate.x ,queueCoordinate.y+1);
                        coordinateDeque.addLast(belowSpot);
                        // Is spot above me part of my island
                        Coordinate aboveSpot = new Coordinate(queueCoordinate.x,queueCoordinate.y-1);
                        coordinateDeque.addLast(aboveSpot);
                   }
                }
            }
        }

        return numberOfIslands;
    }

    public boolean isValidCoordinate(Coordinate c) {
        return c.x >= 0 && c.y >= 0 && c.x < maxXCoordinate && c.y < maxYCoordinate;
    }


    public class Coordinate {
        int x;
        int y;
        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Coordinate)) return false;
            Coordinate that = (Coordinate) o;
            return x == that.x && y == that.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    // Driver Code
    public static void main(String[] args)
    {
       char[][] grid = new char[][]{{'1','1','1','1','0'},{'1','1','0','1','0'},{'1','1','0','0','0'},{'0','0','0','0','0'}};
       NumberOfIslandsBFS m = new NumberOfIslandsBFS();
        m.numIslandsBFS(grid);
    }
}
