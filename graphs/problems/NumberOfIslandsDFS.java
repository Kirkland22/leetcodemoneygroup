package graphs.problems;

import java.util.*;

// https://leetcode.com/problems/number-of-islands/
public class NumberOfIslandsDFS {

    int numberOfIslands = 0;
    Set<String> visited = new HashSet<>();
    public int numIslands(char[][] grid) {
        for(int x = 0; x < grid.length; x++) {
            for(int y = 0; y < grid[0].length; y++) {
                Coordinate currentSpot = new Coordinate(x,y);
                if(grid[x][y] == '1' && !visited.contains(currentSpot.x + "-" + currentSpot.y)) {
                    numberOfIslands++;
                    visited.add(currentSpot.x + "-" + currentSpot.y);

                    // Is spot left of me part of my island
                    Coordinate leftSpot = new Coordinate(currentSpot.x-1,currentSpot.y);
                    findAdjacentLand(leftSpot, grid);

                    // Is spot right of me part of my island
                    Coordinate rightSpot = new Coordinate(currentSpot.x+1 ,currentSpot.y);
                    findAdjacentLand(rightSpot, grid);

                    // Is spot below me part of my island
                    Coordinate belowSpot = new Coordinate(currentSpot.x ,currentSpot.y+1);
                    findAdjacentLand(belowSpot, grid);

                    // Is spot above me part of my island
                    Coordinate aboveSpot = new Coordinate(currentSpot.x,currentSpot.y-1);
                    findAdjacentLand(aboveSpot, grid);
                }
            }
        }

        return numberOfIslands;
    }

    public void findAdjacentLand(Coordinate currentSpot, char[][] grid) {

        //If we are at an invalid space, return
        if(currentSpot.x < 0 || currentSpot.y < 0 || currentSpot.x >= grid.length || currentSpot.y >= grid[0].length) {
            return;
        }

        //If we already visited this spot or this spot is water, return
        if(visited.contains(currentSpot.x + "-" + currentSpot.y) || grid[currentSpot.x][currentSpot.y] == '0') {
            return;
        }

        visited.add(currentSpot.x + "-" + currentSpot.y);

        // Is spot left of me part of my island
        Coordinate leftSpot = new Coordinate(currentSpot.x-1,currentSpot.y);
        findAdjacentLand(leftSpot, grid);

        // Is spot right of me part of my island
        Coordinate rightSpot = new Coordinate(currentSpot.x+1,currentSpot.y);
        findAdjacentLand(rightSpot, grid);

        // Is spot below me part of my island
        Coordinate belowSpot = new Coordinate(currentSpot.x,currentSpot.y+1);
        findAdjacentLand(belowSpot, grid);

        // Is spot above me part of my island
        Coordinate aboveSpot = new Coordinate(currentSpot.x, currentSpot.y-1);
        findAdjacentLand(aboveSpot, grid);
    }


    public class Coordinate {
        int x;
        int y;
        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    // Driver Code
    public static void main(String[] args)
    {
       char[][] grid = new char[][]{{'1','1','1','1','0'},{'1','1','0','1','0'},{'1','1','0','0','0'},{'0','0','0','0','0'}};
       NumberOfIslandsDFS m = new NumberOfIslandsDFS();
       m.numIslands(grid);
    }
}
